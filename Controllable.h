//
// Created by andrew on 18.04.18.
//

#ifndef GAME_BASE_H
#define GAME_BASE_H

#include <SFML/Window.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <cstring>

#define SUP_HEALTH 100
#define SUP_STAMINA 200
#define SUP_MASS 2000
#define SUP_DAMAGE 500
#define SUP_COOLDOWN 5
#define pure = 0
const int INF_MASS = -1;

class Controllable {
protected:
    sf::Vector2f coord;
    float height = 0;
    float width = 0;

    sf::Sprite sprite_;
    char *name = nullptr;

    double mass = INF_MASS;

public:
    Controllable(char *name) : name(name) {
        sf::Texture &texture = GetTexture();
        sf::Sprite sprite(texture);
        sprite_ = sprite;
        width = texture.getSize().x;
        height = texture.getSize().y;
        sprite_.setOrigin(width / 2, height / 2);
    }

    virtual void move() pure;

    virtual void draw() pure;

    virtual ~Controllable() {
        free(name);

    }

    explicit Controllable() = default;

    sf::Texture &GetTexture();

    bool operator==(const Controllable &that) {
        return coord.x == that.coord.x && that.coord.y == coord.y && !strcmp(name, that.name);
    }

    bool operator!=(const Controllable &that) {
        return !(*this == that);
    }

};


class Movable : public Controllable {
protected:
    sf::Vector2f v;
    size_t vMax = 0;
    sf::Vector2f a;
    float angle;
    sf::Vector2i dest;
    float stamina;

public:
    void move() override;

    void draw() override;

    virtual void rotate() pure;

    void collapse(Controllable &Obj);

    void collapse(Movable &member);

    Movable(char *name) : Controllable(name) {};
};

class Character : public Movable {
protected:
    float health;
    float std_damage;
    float cooldown;
    float stamina;
    enum missle_type {
        Bullet = 1,
        Rocket,
        Lazer

    };
public:
    void draw() override;

    void move() override;

    virtual void logic() pure;

    Character(char *name) : Movable(name) {};
};

class Spacecraft : public Character {
public:
    Spacecraft(char *name) : Character(name) {
        health = SUP_HEALTH;
        cooldown = SUP_COOLDOWN;
        stamina = SUP_STAMINA;
        std_damage = SUP_DAMAGE;
        mass = SUP_MASS;

    }

    void control() override;


};


#endif //GAME_BASE_H
